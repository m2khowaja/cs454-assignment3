#pragma once
#include <iostream>
#include <vector>

using namespace std;

class Communication {
public:
	int messageType;	
	char serverAddress[256];
	int serverPort;
	char name[64];
	int returnCode;
	void **args = nullptr;
	vector <int> argTypes;
	Communication();
	Communication(int type, string address, int port, string functionName, int *arg_types);
	~Communication();
	Communication(int type);
	Communication(int type, int code);
	Communication(int type, string functionName, int *arg_types);
	Communication(int type, string functionName, int *arg_types, void ** p_args);
	Communication(int type, string address, int port);
};
