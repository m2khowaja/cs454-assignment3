#include <stdio.h>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <errno.h>
#include <sstream>
#include <map>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include "utility.h"

int initSocket (int &sockfd, string &host, int &port) {
	char hname[512];
    struct addrinfo hints, *servinfo, *p;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // use my IP address
    int option = 1;

    int rv = getaddrinfo(NULL, "0", &hints, &servinfo);
    if (rv != 0) {
        // cerr << "getaddrinfo error: "<< gai_strerror(rv) << "." << endl;
        return -1;
    }

    // Try each address
    // Source: http://man7.org/linux/man-pages/man3/getaddrinfo.3.html, 
    //http://my.fit.edu/~vkepuska/ece3551/ADI_Speedway_Golden/Blackfin%20Speedway%20Manuals/LwIP/socket-api/setsockopt_exp.html, http://long.ccaba.upc.edu/long/045Guidelines/eva/ipv6.html
    for (p = servinfo; p != nullptr; p = p->ai_next) {
    	sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
    	if (sockfd == -1) {
    		continue;
    	}

    	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(int)) == -1) {
    		// cerr << "Setsockopt " << endl;
    		return -2;
    	}
    	if (bind(sockfd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
    		close (sockfd);
    		continue;
    	} else {
    		gethostname (hname, sizeof hname);
      		// cout << "SERVER_ADDRESS " << hname << endl;
      		host = string(hname);

    		struct sockaddr_storage addr;
      		socklen_t len = sizeof addr;

      		getsockname(sockfd, (struct sockaddr*) &addr, &len);
      		if (addr.ss_family == AF_INET) {
      			struct sockaddr_in *sin = (struct sockaddr_in*) &addr;
      			port = ntohs(sin->sin_port);
      		} else { // ipv6
      			struct sockaddr_in6 *sin = (struct sockaddr_in6*) &addr;
      			port = ntohs(sin->sin6_port);
      		}
      		// cout << "SERVER_PORT " << ntohs(sin.sin_port) << endl;
    	}
    	break;
    }

    freeaddrinfo(servinfo);

    if (p == nullptr) {
    	// cerr << "p is null" << endl;
    	return -3;
    }
    rv = listen(sockfd, BACKLOG);

    if (rv == -1) {
    	// cerr << "Error on listen" << endl;
    	return -4;
    }

    return 0;
}

int establishConnection (int &sockfd, string host, string port) {
	struct addrinfo hints, *servinfo, *p;

	const char * destinationHost = host.c_str();
	const char * destinationPort = port.c_str();

	if (destinationHost == nullptr || destinationPort == nullptr) {
		return -1;
	}

	memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    // hints.ai_flags = AI_PASSIVE; // use my IP address


	int rv = getaddrinfo(destinationHost, destinationPort, &hints, &servinfo);

	if (rv != 0) {
		// cerr << "establishConnection getaddrinfo" << endl;
		return -2;
	}

	for (p = servinfo; p != nullptr; p = p->ai_next) {
		sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
    	if (sockfd == -1) {
    		continue;
    	}

    	if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
	        close(sockfd);
	        continue;
	    }

	    break; // if we get here, we must have connected successfully
	}

	if (p == nullptr) {
	    // looped off the end of the list with no connection
	    //cerr << "failed to connect" << endl;
	    return -3;
	}

	freeaddrinfo(servinfo);
	return 0;
}


int closeConnection (int sockfd) {
	close (sockfd);
    return 0;
}

template <class T>
void marshallType(void * & pointer, T *value, int size) {
    T *j = (T *)pointer;
    for (int i = 0; i < size; i++) {
        *j = value[i];
        j++;
    }
    pointer = (void *) j;
}

template <class T>
void unmarshallType(void * & pointer, T * & value, int size) {
    value = new T[size];
    T *j = (T *)pointer;
    for (unsigned int i = 0; i < size; i++) {
        value[i] = *j;
        j++;
    }
    pointer = (void *)j ;
}


int sendCommunication(int sockfd, int type, Communication & communicateMessage){
    int size = communicateMessage.argTypes.size() * 4;

    if (communicateMessage.args != nullptr) {
        for (unsigned int i = 0; i < communicateMessage.argTypes.size(); i++){
            int argType = (communicateMessage.argTypes[i] >> 16) & 0xff;
            int argSize = (communicateMessage.argTypes[i] << 16) >> 16;
            if (argSize <= 0){
                argSize = 1;
            }
            if (argType == ARG_CHAR){
                size = size + argType * argSize;
            }
            else if (argType == ARG_SHORT) {
                size = size + argType * argSize;
            }
            else if (argType == ARG_INT) {
                size = size + 4 * argSize;
            }
            else if (argType == ARG_FLOAT) {
                size = size + 4 * argSize;
            }
            else if (argType == ARG_LONG) {
                size = size + 8 * argSize;
            }
            else if (argType == ARG_DOUBLE) {
                size = size + 8 * argSize;
            }
        }
    }

    size+=332;

    char *marshalled = new char[size];
    Communication * communicateMessagePointer = new Communication(communicateMessage);

    // Marshall the message
    int *l = (int*)marshalled;
    *l++ = communicateMessagePointer -> messageType;
    *l++ = communicateMessagePointer -> returnCode;
    *l++ = communicateMessagePointer -> serverPort;

    char *c = (char*) l;
    for (unsigned int i = 0; i < 256; i++){
        *c++ = communicateMessagePointer -> serverAddress[i];
    }

    for (unsigned int i = 0; i < 64; i++){
        *c++ = communicateMessagePointer -> name[i];
    }

    int *j = (int*)c;
    for (unsigned int i = 0; i < communicateMessagePointer -> argTypes.size(); i++){
        *j++ = communicateMessagePointer -> argTypes[i];
    }

    if (communicateMessagePointer -> args != nullptr) {
        void * k = (void *) j;
        for (int i = 0; i < communicateMessagePointer -> argTypes.size() -1; i++) {
            int argType = (communicateMessagePointer->argTypes[i] >> 16) & 0xff;
            int argSize = (communicateMessagePointer->argTypes[i] << 16) >> 16;
            if (argSize <= 0){
                argSize = 1;
            }
            if (argType == ARG_CHAR){
                marshallType(k, (char*) communicateMessagePointer -> args[i], argSize);
            }
            else if (argType == ARG_SHORT) {
                marshallType(k, (short*) communicateMessagePointer -> args[i], argSize);
            }
            else if (argType == ARG_INT) {
                marshallType(k, (int*) communicateMessagePointer -> args[i], argSize);
            }
            else if (argType == ARG_FLOAT) {
                marshallType(k, (float*) communicateMessagePointer -> args[i], argSize);
            }
            else if (argType == ARG_LONG) {
                marshallType(k, (long*) communicateMessagePointer -> args[i], argSize);
            }
            else if (argType == ARG_DOUBLE) {
                marshallType(k, (double*) communicateMessagePointer -> args[i], argSize);
            }
        }
    }

    //Send
    int *sendData = new int[2];
    sendData[0] = size;
    sendData[1] = type;
    int rv = send(sockfd, sendData, 8, 0);
    delete sendData;

    if (rv == -1) {
        // cerr << "Failed to send message length and type" << endl;
        delete [] marshalled;
        delete communicateMessagePointer;
        return -1;
    }
    if (send(sockfd, marshalled, size, 0) == -1){
        // cerr << "Failed to send message" << endl;
        delete [] marshalled;
        delete communicateMessagePointer;
        return -1;
    }

    delete [] marshalled;
    delete communicateMessagePointer;
    return 0;
}

int receiveCommunication (int sockfd, int &size, int &type, Communication &communicateMessage){
    int *data = new int[2];
    if (recv(sockfd, data, 8, 0) <= 0){
        delete [] data;
        return -1;
    }

    size = data[0];
    type = data[1];
    
    char * marshalled = new char[size];

    int rv = recv(sockfd, marshalled, size, 0);
    
    // Unmarshall the message
    Communication *communicateMessagePointer = &communicateMessage;
    int *l = (int*)marshalled;
    communicateMessagePointer -> messageType = *l++;
    communicateMessagePointer -> returnCode = *l++;
    communicateMessagePointer -> serverPort = *l++;

    char *c = (char*) l;
    for (unsigned int i = 0; i < 256; i++){
        communicateMessagePointer -> serverAddress[i] = *c++;
    }

    for (unsigned int i = 0; i < 64; i++){
        communicateMessagePointer -> name[i] = *c++;
    }

    int *j = (int*)c;
    for (; *j != 0;){
        communicateMessagePointer -> argTypes.push_back(*j++);
    }
    communicateMessagePointer -> argTypes.push_back(0);
    j++;

    if (communicateMessagePointer -> messageType == EXECUTE || communicateMessagePointer -> messageType == EXECUTE_SUCCESS) {
        if (communicateMessagePointer -> args == nullptr) {
            communicateMessagePointer -> args = new void * [communicateMessagePointer->argTypes.size()-1];
        }
        void * k = (void *) j;
        for (unsigned int i = 0; i < communicateMessagePointer -> argTypes.size() -1; i++) {
            int argType = (communicateMessagePointer->argTypes[i] >> 16) & 0xff;
            int argSize = (communicateMessagePointer->argTypes[i] << 16) >> 16;
            if (argSize <= 0){
                argSize = 1;
            }
            if (argType == ARG_CHAR){
                char *temp_arg_for_unmarshall = (char *) communicateMessagePointer -> args[i];
                unmarshallType(k, temp_arg_for_unmarshall, argSize);
                communicateMessagePointer -> args[i] = (void *) temp_arg_for_unmarshall;
            }
            else if (argType == ARG_SHORT) {
                short *temp_arg_for_unmarshall = (short *) communicateMessagePointer -> args[i];
                unmarshallType(k, temp_arg_for_unmarshall, argSize);
                communicateMessagePointer -> args[i] = (void *) temp_arg_for_unmarshall;
            }
            else if (argType == ARG_INT) {
                int *temp_arg_for_unmarshall = (int *) communicateMessagePointer -> args[i];
                unmarshallType(k, temp_arg_for_unmarshall, argSize);
                communicateMessagePointer -> args[i] = (void *) temp_arg_for_unmarshall;
            }
            else if (argType == ARG_FLOAT) {
                float *temp_arg_for_unmarshall = (float *) communicateMessagePointer -> args[i];
                unmarshallType(k, temp_arg_for_unmarshall, argSize);
                communicateMessagePointer -> args[i] = (void *) temp_arg_for_unmarshall;
            }
            else if (argType == ARG_LONG) {
                long *temp_arg_for_unmarshall = (long *) communicateMessagePointer -> args[i];
                unmarshallType(k, temp_arg_for_unmarshall, argSize);
                communicateMessagePointer -> args[i] = (void *) temp_arg_for_unmarshall;
            }
            else if (argType == ARG_DOUBLE) {
                double *temp_arg_for_unmarshall = (double *) communicateMessagePointer -> args[i];
                unmarshallType(k, temp_arg_for_unmarshall, argSize);
                communicateMessagePointer -> args[i] = (void *) temp_arg_for_unmarshall;
            }
        }
    }


    delete [] marshalled;
    
    if (rv <= 0){
        return -1;
    }
    return rv;
}