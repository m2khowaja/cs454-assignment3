#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>
#include "communication.h"

Communication::Communication(){

}

Communication::Communication(int type, string address, int port, string functionName, int *arg_types) : messageType(type), serverPort(port) {
	strcpy(serverAddress, address.c_str());
	strcpy(name, functionName.c_str());

	int *curr = arg_types;

	for (; curr != nullptr && *curr != 0; curr++){
		argTypes.push_back(*curr);
	}
	argTypes.push_back(0);
	// Register
}


Communication::~Communication(){

}

Communication::Communication(int type) : messageType(type) {
	// Terminate
}

Communication::Communication(int type, int code) : messageType(type), returnCode(code) {
	// Register_Success, Register_Failure, Loc_Failure, Execute_Failure
}

Communication::Communication(int type, string functionName, int *arg_types) : messageType(type) {
	strcpy(name, functionName.c_str());
	int *curr = arg_types;

	for (; curr != nullptr && *curr != 0; curr++){
		argTypes.push_back(*curr);
	}
	argTypes.push_back(0);

	// LOC_REQUEST
}

Communication::Communication(int type, string functionName, int *arg_types, void ** p_args) : messageType(type), args(p_args) {
	strcpy(name, functionName.c_str());
	int *curr = arg_types;

	for (; curr != nullptr && *curr != 0; curr++){
		argTypes.push_back(*curr);
	}
	argTypes.push_back(0);
	// Execute and Execute_Success
}

Communication::Communication(int type, string address, int port) : messageType(type), serverPort(port) {
	strcpy(serverAddress, address.c_str());
	// Loc_Success
}

