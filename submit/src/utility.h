#pragma once

#include <string>
#include <vector>
#include "communication.h"

using namespace std;

#define ARG_CHAR    1
#define ARG_SHORT   2
#define ARG_INT     3
#define ARG_LONG    4
#define ARG_DOUBLE  5
#define ARG_FLOAT   6

#define ARG_INPUT   31
#define ARG_OUTPUT  30

#define BACKLOG 10
#define MESSAGE_BC 0
#define MESSAGE_CS 1
#define MESSAGE_BS 2

#define REGISTER 50
#define REGISTER_SUCCESS 51
#define REGISTER_FAILURE 52

#define LOC_REQUEST 53
#define LOC_SUCCESS 54
#define LOC_FAILURE 55

#define EXECUTE 56
#define EXECUTE_SUCCESS 57
#define EXECUTE_FAILURE 58

#define TERMINATE 59

typedef int (*skeleton)(int *, void **);

// Sockets
int initSocket (int &sockfd, string &host, int &port);

int establishConnection (int &sockfd, string host, string port);

int closeConnection (int sockfd);

int sendCommunication(int sockfd, int type, Communication & communicateMessage);

int receiveCommunication (int sockfd, int &size, int &type, Communication &communicateMessage);
