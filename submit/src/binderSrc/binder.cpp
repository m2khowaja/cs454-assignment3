#include <stdio.h>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <errno.h>
#include <map>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include "binder.h"
#include "../utility.h"

bool exitBinder = false;
int roundRobin = -1;


void roundRobinRotation (string functionName, int id, string& serverName, int& serverPort) {
    Storage storage = binderDatabase[functionName][id];
    roundRobin +=1;
    int index = roundRobin % binderServerNames.size();

    for (;;index = (index+1) % binderServerNames.size()) {
        for (unsigned int i = 0; i < storage.serverNames.size(); i++) {
            if (storage.serverNames[i] == binderServerNames[index]) {
                if (storage.serverPorts[i] == binderServerPorts[index]) {
                    roundRobin = index;
                    serverName = storage.serverNames[i];
                    serverPort = storage.serverPorts[i];
                    return;
                }
            }
        }
    }
}

void init() {
	string host = "";
	int port = 0;
	initSocket(sockfd, host, port);
	cout << "BINDER_ADDRESS " << host << endl;
	cout << "BINDER_PORT " << port << endl;
}

int receiveMessage (int fd) {
    // Make a call to receive message
    // Store contents in map of buffers and update sizes and types
    int size = 0;
    int type = 0;
    Communication communication;

    int rv = receiveCommunication(fd, size, type, communication);

    if (rv >= 0){
        sizes[fd] = size;
        types[fd] = type;
        communications [fd] = communication;
    }
    return rv;
}

void accept() {
    int newfd;        // newly accept()ed socket descriptor
    socklen_t addrlen;
    struct sockaddr_storage remoteaddr;
    fd_set master;
    fd_set read_fds;
    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    FD_SET(sockfd, &master);

    // keep track of the biggest file descriptor
    int fdmax = sockfd;

    // main loop
    while (true) {

        if (exitBinder && binderServerNames.size() == 0) {
            break;
        }
        read_fds = master; // copy it
        if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
            //cerr << "Error on select" << endl;
            return;
        }

         // run through the existing connections looking for data to read
        for(unsigned int i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &read_fds)) {
                if (i == sockfd) {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
                    if ((newfd = accept(sockfd, (struct sockaddr *)&remoteaddr,
                                                             &addrlen)) == -1) {
                        //cerr << "Error accepting socket " << sockfd << endl;
                        // close(sockfd);
                    } else {
                        FD_SET(newfd, &master); // add to master set
                        sizes[newfd] = 0;
                        if (newfd > fdmax) {    // keep track of the maximum
                            fdmax = newfd;
                        }
                    }
                } else {
                    // handle data from a client
                    int rv = receiveMessage(i);
                    if (rv < 0) {
                        processMessage(i, false);
                        sizes.erase(i);
                        types.erase(i);

                        // Erase message from buffers
                        buffers.erase(i);
                        communications.erase(i);
                        
                        close(i);
                        FD_CLR(i, &master); // remove from master set
                    } else {
                    	// Process the message: can be client or server
                        processMessage(i, true);
                    }
                }
            }
        }
    }
}

void _terminate() {
	close(sockfd);
}


void processMessage (int fd, bool isConnected) {
    if (types[fd] == MESSAGE_BC) {
        processClient(fd, isConnected);

    } else if (types[fd] == MESSAGE_BS) {
        processServer(fd, isConnected);
    }
}

void processClient(int fd, bool isConnected){
    if (!isConnected) return;

    if (communications[fd].messageType == TERMINATE) {
        exitBinder = true;
        for (unsigned int i = 0; i < binderServerNames.size(); i++) {
            int temp;
            establishConnection(temp, binderServerNames[i], to_string(binderServerPorts[i]));
            Communication terminateCommunication(TERMINATE);
            sendCommunication(temp, MESSAGE_BS, terminateCommunication);
        }
        return;
    }    
    string functionName = communications[fd].name;
    vector<int> argTypes = communications[fd].argTypes;

    int id = findFunction(functionName, argTypes);
    string serverName = "";
    int serverPort = 0;
    if (id >= 0) {
        roundRobinRotation(functionName, id, serverName, serverPort);

        Communication successMessage(LOC_SUCCESS, serverName, serverPort);
        sendCommunication (fd, MESSAGE_CS, successMessage);
    }
    else {
        Communication failureMessage(LOC_FAILURE, -1);
        sendCommunication (fd, MESSAGE_CS, failureMessage);
    }
}

int findFunction (string functionName, vector<int> argTypes) {
    if (binderDatabase.find(functionName) == binderDatabase.end()) {
        return -1;
    }
    for (unsigned int i = 0; i < binderDatabase[functionName].size(); i++) {
        Storage cell = binderDatabase[functionName][i];
        if (cell.equalTo(functionName, argTypes)) {
            return i;
        }
    }
    return -1;
}

void processServer(int fd, bool isConnected){
    Communication communication = communications[fd];
    
    if (!isConnected) {
        string serverName = communication.serverAddress;
        int serverPort = communication.serverPort;
        for (auto& keys : binderDatabase) {
            for (Storage& values : keys.second) {
                for (unsigned int i = 0; i < values.serverNames.size(); i++){
                    if (serverName == values.serverNames[i] && serverPort == values.serverPorts[i]) {
                        values.serverNames.erase(values.serverNames.begin() + i);
                        values.serverPorts.erase(values.serverPorts.begin() + i);
                    }
                }
            }
        }
        for (unsigned int i=0; i < binderServerNames.size(); i++) {
            if (binderServerNames[i] == serverName && binderServerPorts[i] == serverPort) {
                binderServerNames.erase(binderServerNames.begin() + i);
                binderServerPorts.erase(binderServerPorts.begin() + i);
                if (i <= roundRobin) {
                    roundRobin = (roundRobin + binderServerNames.size()) % (binderServerNames.size() + 1);
                }
                break;
            }
        }
    }

    string functionName = communication.name;
    vector<int> argTypes = communication.argTypes;
    int registerReturnCode = 0;
    if (exitBinder) {
        registerReturnCode = -10;
    } else {
        int id = findFunction(functionName, argTypes);
        try {
            if (id >= 0) {
                bool exists = false;
                for (unsigned i = 0; i < binderDatabase[functionName][id].serverNames.size(); i++){
                    if (binderDatabase[functionName][id].serverNames[i] == communication.serverAddress && binderDatabase[functionName][id].serverPorts[i] == communication.serverPort) {
                        exists = true;
                        registerReturnCode = 1;
                        break;
                    }
                }
                if (!exists) {
                    binderDatabase[functionName][id].serverNames.push_back(communication.serverAddress);
                    binderDatabase[functionName][id].serverPorts.push_back(communication.serverPort);    
                }
            } else {
                Storage storage (functionName, argTypes);
                storage.serverNames.push_back(communication.serverAddress);
                storage.serverPorts.push_back(communication.serverPort);
                
                binderDatabase[functionName].push_back(storage);
            }
            if (registerReturnCode == 0) {
                bool exists = false;
                for (unsigned int i = 0; i < binderServerNames.size(); i++) {
                    if (binderServerNames[i] == communication.serverAddress && binderServerPorts[i] == communication.serverPort) {
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    binderServerNames.push_back(communication.serverAddress);
                    binderServerPorts.push_back(communication.serverPort);
                }
            }    
        } catch (...) {
            // FAILED
            registerReturnCode = -100;
            Communication communicateFailure(REGISTER_FAILURE, registerReturnCode);
            sendCommunication(fd, MESSAGE_BC, communicateFailure);
            return;
        }    
    }
    
    Communication communicateSuccess(REGISTER_SUCCESS, registerReturnCode);
    sendCommunication(fd, MESSAGE_BC, communicateSuccess);
}


int main() {
	init();
	accept();
	_terminate();
}
