#pragma once

#include <map>
#include <vector>
#include "../communication.h"
#include "../storage.h"

using namespace std;

// Variables
int sockfd;
map<int, int> sizes;
map<int, char*> buffers;
map<int, int> types;
map<int, int> sources;
map<int, Communication> communications;
map<string, vector<Storage>> binderDatabase;
vector<string> binderServerNames;
vector<int> binderServerPorts;

// Functions
void init();

void accept();

void _terminate();

int receiveMessage (int fd);

void processMessage(int fd, bool isConnected);

void processClient(int fd, bool isConnected);

void processServer(int fd, bool isConnected);

int findFunction(string functionName, vector<int> argTypes);

void roundRobinRotation (string functionName, int id, string& serverName, int& serverPort);