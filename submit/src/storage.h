#pragma once
#include <iostream>
#include <vector>
#include <map>

#include "utility.h"

using namespace std;

class Storage {
public:
	string name;
	vector <int> argTypes;
	vector<string> serverNames;
	vector<int> serverPorts;
	skeleton functionSkeleton;

	Storage (string fname, vector<int> types);
	bool equalTo(string otherName, vector<int> otherTypes);
};