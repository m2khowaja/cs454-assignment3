#include <iostream>
#include "../utility.h"
#include "../communication.h"
#include "../storage.h"
#include <string>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <map>
#include <vector>
#include <netdb.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>

using namespace std;

int clientSocketfd;
int binderSocketfd;
string serverAddress = "";
int clientPort = 0;

map<string, vector<Storage>> serverDatabase;

map<int, int> sizes;
map <int, int> types;
map<int, Communication> communications;

pthread_mutex_t serverThread;
vector<pthread_t> listOfThreads;
int serverAlive = 0;

bool isTerminating;

extern "C" int rpcInit() {

    if (getenv("BINDER_ADDRESS") == NULL || getenv("BINDER_PORT") == NULL) {
        return -1;
    }

    // int sockfd;
    // string host = "";
    // int port = 0;
    if (serverAlive > 0) {
        return 1;
    }

    serverAlive = 1;

    string address = getenv("BINDER_ADDRESS");
    string port = getenv("BINDER_PORT");

    pthread_mutex_init(&serverThread, NULL);

    int rv = initSocket(clientSocketfd, serverAddress, clientPort);

    if (rv < 0) {
        return -2;
    }

    if (serverAddress == "" || clientPort == 0) {
        return -3;
    }

    rv = establishConnection(binderSocketfd, address, port);

    if (rv < 0) {
        return -4;
    }

    return 0;
}

extern "C" int rpcCall(char* name, int* argTypes, void** args) {
    if (getenv("BINDER_ADDRESS") == NULL || getenv("BINDER_PORT") == NULL) {
        return -5;
    }
    string address = getenv("BINDER_ADDRESS");
    string port = getenv("BINDER_PORT");
    int sockfd;

    int rv = establishConnection(sockfd, address, port);
    if (rv < 0) {
        return -6;
    }

    string name_string = name;

    Communication toSend(LOC_REQUEST, name_string, argTypes);

    if (sendCommunication(sockfd, MESSAGE_BC, toSend) < 0){
        closeConnection(sockfd);
        return -7;
    }

    Communication toReceive;
    int size;
    int type;
    if (receiveCommunication(sockfd, size, type, toReceive) < 0){
        closeConnection(sockfd);
        return -8;
    }

    closeConnection(sockfd);

    if (toReceive.messageType == LOC_FAILURE) {
        return -9;
    }

    string serverName = toReceive.serverAddress;
    int serverPort = toReceive.serverPort;


    rv = establishConnection(sockfd, serverName, to_string(serverPort));
    if (rv < 0) {
        return -10;
    }

    Communication executeCommunication(EXECUTE, name_string, argTypes, args);

    rv = sendCommunication(sockfd, MESSAGE_CS, executeCommunication);
    if (rv < 0) {
        closeConnection(sockfd);
        return -11;
    }

    Communication responseCommunication;
    responseCommunication.args = args;

    if (receiveCommunication(sockfd, size, type, responseCommunication) < 0){
        closeConnection(sockfd);
        return -12;
    }

    closeConnection(sockfd);

    return 1;
}

extern "C" int rpcCacheCall(const char* name, int* argTypes, void** args) {
    return 1;
}

extern "C" int rpcRegister(char* name, int* argTypes, skeleton f) {
    if (serverAlive <= 0) {
        return -13;
    }
    if (serverAlive > 2) {
        return -14;
    }

    serverAlive = 2;

    string functionName = name;

    Communication communicateMessage(REGISTER, serverAddress, clientPort, functionName, argTypes);

    int rv = sendCommunication(binderSocketfd, MESSAGE_BS, communicateMessage);
    if (rv < 0) {
        return -15;
    }
    Communication communicationToReceive;
    int receiveSize;
    int receiveType;
    if (rv >=0) {
        rv = receiveCommunication(binderSocketfd, receiveSize, receiveType, communicationToReceive);
        if (communicationToReceive.messageType == REGISTER_SUCCESS && rv >= 0){
            pthread_mutex_lock(&serverThread);
            vector<int> argTypes = communicateMessage.argTypes;
            Storage storage (functionName, communicateMessage.argTypes);
            int id;
            if (serverDatabase.find(functionName) == serverDatabase.end()) {
                id = -1;
            } else {
                id = -1;
                for (unsigned int i = 0; i < serverDatabase[functionName].size(); i++) {
                    if (serverDatabase[functionName][i].equalTo(functionName, argTypes)) {
                        id = i;
                        break;
                    }
                }
            }
            if (id < 0) {
                storage.functionSkeleton = f;
                serverDatabase[functionName].push_back(storage);
            } else {
                serverDatabase[functionName][id].functionSkeleton = f;
            }
            pthread_mutex_unlock(&serverThread);
        } else {
            return -16;
        }

        if (communicationToReceive.messageType == REGISTER_FAILURE) {
            rv = communicationToReceive.returnCode;
        }
    } 
    if (rv < 0) {
        return -17;
    }

    return rv;
}

int receiveMessage(int fd){
    Communication toReceive;
    int size;
    int type;
    int rv = receiveCommunication(fd, size, type, toReceive);
    if (rv >= 0){
        sizes[fd] = size;
        types[fd] = type;
        communications[fd] = toReceive;
    }
    return rv;
}

void *processClient (void * fdptr) {
    int fd = *reinterpret_cast<int *> (fdptr);
    skeleton functionSkeleton;
    string functionName = communications[fd].name;
    vector <int> argTypes = communications[fd].argTypes;
    pthread_mutex_lock(&serverThread);
    Storage storage(functionName, argTypes);

    int id;
    int rv;
    if (serverDatabase.find(functionName) == serverDatabase.end()) {
        id = -1;
    } else {
        id = -1;
        for (unsigned int i = 0; i < serverDatabase[functionName].size(); i++) {
            if (serverDatabase[functionName][i].equalTo(functionName, argTypes)) {
                id = i;
                break;
            }
        }
    }
    rv = -1;
    if (id >= 0) {
        functionSkeleton = serverDatabase[functionName][id].functionSkeleton;
    }
    pthread_mutex_unlock(&serverThread);

    if (id >=0) {
        void ** args = communications[fd].args;
        int * functionArgTypes = new int [communications[fd].argTypes.size()];
        for (unsigned int i = 0; i < communications[fd].argTypes.size(); i++){
            functionArgTypes[i] = communications[fd].argTypes[i];
        }

        rv = functionSkeleton(functionArgTypes, args);

        if (rv >= 0) {
            Communication reply(EXECUTE_SUCCESS, functionName, functionArgTypes, args);
            if (sendCommunication(fd, MESSAGE_CS, reply) < 0){
                rv = -1;
            }
        }
    }

    if (rv < 0) {
        Communication sendFailure(EXECUTE_FAILURE, -1);
        if (sendCommunication(fd, MESSAGE_CS, sendFailure) < 0){
            rv = -1;
        }
    }

    if (communications[fd].args != nullptr) {
        for (unsigned int i = 0; i < communications[fd].argTypes.size()-1; i++) {
            if (communications[fd].args[i] != nullptr) {
                int argType = (communications[fd].argTypes[i] >> 16) & 0xff;
                if (argType == ARG_CHAR){
                    delete [] (char *) communications[fd].args[i];
                }
                else if (argType == ARG_SHORT) {
                    delete [] (short *) communications[fd].args[i];
                }
                else if (argType == ARG_INT) {
                    delete [] (int *) communications[fd].args[i];
                }
                else if (argType == ARG_FLOAT) {
                    delete [] (float *) communications[fd].args[i];
                }
                else if (argType == ARG_LONG) {
                    delete [] (long *) communications[fd].args[i];
                }
                else if (argType == ARG_DOUBLE) {
                    delete [] (double *) communications[fd].args[i];
                }
            }
        }
        delete [] communications[fd].args;
    }
    pthread_exit(NULL);
}


void processMessage (int fd) {
    if (types[fd] == MESSAGE_CS) {
        pthread_t threadClient;
        pthread_create(&threadClient, NULL, processClient, &fd);
        listOfThreads.push_back(threadClient);
        // processClient(fd);
    } else if (types[fd] == MESSAGE_BS) {
        if (communications[fd].messageType == TERMINATE) {
            for (unsigned int i = 0; i < listOfThreads.size(); i++) {
                pthread_join(listOfThreads[i], 0);
            }
        }
        isTerminating = true;
    }
}

extern "C" int rpcExecute() {
    if (serverAlive < 2) {
        return -18;
    } else {
        serverAlive = 3;
    }
    isTerminating = false;
    int newfd;        // newly accept()ed socket descriptor
    socklen_t addrlen;
    struct sockaddr_storage remoteaddr;
    fd_set master;
    fd_set read_fds;
    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    FD_SET(clientSocketfd, &master);

    // keep track of the biggest file descriptor
    int fdmax = clientSocketfd;

    // main loop
    while (true) {
        if (isTerminating) {
            break;
        }
        read_fds = master; // copy it
        if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
            exit(3);
        }

         // run through the existing connections looking for data to read
        for(unsigned int i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &read_fds)) {
                if (i == clientSocketfd) {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
                    if ((newfd = accept(clientSocketfd, (struct sockaddr *)&remoteaddr,
                                                             &addrlen)) == -1) {
                        //cerr << "Error accepting socket " << clientSocketfd << endl;
                        // close(sockfd);
                    } else {
                        FD_SET(newfd, &master); // add to master set
                        sizes[newfd] = 0;
                        if (newfd > fdmax) {    // keep track of the maximum
                            fdmax = newfd;
                        }
                    }
                } else {
                    // handle data from a client
                    int rv = receiveMessage(i);
                    if (rv < 0) {
                        sizes.erase(i);
                        types.erase(i);
                        communications.erase(i);
                        // Erase message from buffers
                        close(i);
                        FD_CLR(i, &master); // remove from master set
                    } else {
                        processMessage(i);
                    }
                }
            }
        }
    }
    close(clientSocketfd);
    close(binderSocketfd);
    serverAlive = 2;
    return 0;
}

extern "C" int rpcTerminate() {
    if (getenv("BINDER_ADDRESS") == NULL || getenv("BINDER_PORT") == NULL) {
        return -19;
    }
    int fd;
    string address = getenv("BINDER_ADDRESS");
    string port = getenv("BINDER_PORT");

    int rv = establishConnection(fd, address, port);
    if (rv < 0) {
        return -20;
    }
    
    Communication terminateCommunication(TERMINATE);

    rv = sendCommunication(fd, MESSAGE_BC, terminateCommunication);
    if (rv < 0) {
        closeConnection(fd);
        return -21;
    }
    return 0;
}
