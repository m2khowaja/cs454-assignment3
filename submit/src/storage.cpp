#include "storage.h"

using namespace std;

Storage::Storage (string fname, vector<int> types) : name(fname), argTypes(types) {

}

bool Storage::equalTo(string otherName, vector<int> otherTypes) {
	if (name != otherName || argTypes.size() != otherTypes.size()) {
		return false;
	}

	vector <int> myArgs;
	vector <int> otherArgs;
	vector <int> myOutput;
	vector <int> otherOutput;

	for (unsigned int i = 0; i < argTypes.size()-1; i++){
		int a = (argTypes[i] >> 16) << 16;
		int b = (otherTypes[i] >> 16) << 16;

		if (a < 0) {
			myArgs.push_back(a);
		} else{
			myOutput.push_back(a);
		}
		if (b < 0) {
			otherArgs.push_back(b);
		} else{
			otherOutput.push_back(b);
		}
	}

	myArgs.insert(myArgs.begin(), myOutput.begin(), myOutput.end());
	myArgs.push_back(0);
	otherArgs.insert(otherArgs.begin(), otherOutput.begin(), otherOutput.end());
	otherArgs.push_back(0);

	for (unsigned int i = 0; i < myArgs.size(); i++) {
		if (myArgs[i] != otherArgs[i]) {
			return false;
		}
	}
	return true;
}