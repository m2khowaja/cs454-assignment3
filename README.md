CS454 Assignment 3

Mustaqeem Khowaja - m2khowaj
Surudh Bhutani - s2bhutan

Run make in the src directory to create binder executable and static library librpc.a

If you want to run the test client, run make in the ./PartialTestcode directory

Run ./src/binder executable

Set BINDER_ADDRESS and BINDER_PORT environment variables

Run the client and the server.

